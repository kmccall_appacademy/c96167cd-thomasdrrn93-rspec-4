class Book

  attr_reader :title

  def title=(title)
    little_words = ["and", "or", "the", "is", "a", "in", "of", "or", "an"]

    split = title.downcase.split

    split = split.map.with_index do |word, idx|
      if little_words.include?(word) && idx != 0
        word
      else
        word.capitalize
      end
    end
    @title = split.join(" ")
  end

end
