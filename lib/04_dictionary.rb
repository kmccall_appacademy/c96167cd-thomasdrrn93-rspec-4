class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(variable)
    if variable.is_a?(String)
      @entries[variable] = nil
    else
      @entries.merge!(variable)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(variable)
    @entries.has_key?(variable)
  end

  def find(variable)
    @entries.select  { |k, v| k.match(variable) }
  end

  def printable
    self.keywords.map do |k|
      %Q{[#{k}] "#{@entries[k]}"}
    end.join("\n")
  end   
end
