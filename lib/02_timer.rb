class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end
  def pad(digit)
    if digit < 10
      "0#{digit.to_s}"
    else
      "#{digit.to_s}"
    end
  end
  def secondsdigit
    seconds % 60
  end
  def minutes
    seconds % 3600 / 60
  end
  def hours
    seconds / 3600
  end
  def time_string
    pad(hours) + ":" + pad(minutes) + ":" + pad(secondsdigit)
  end
end
